import Home from './pages/Home.vue';
import Products from './pages/Products.vue';
import ProductsCreate from './pages/ProductsCreate.vue';
import VideoLink from './pages/VideoLink.vue';

export default [
    {
        path: '/home',
        name: 'home',
        component: Home,
    },

    {
        path: '/products',
        name: 'products',
        component: Products,
    },

    {
        path: '/products/create',
        name: 'products.create',
        component: ProductsCreate,
    },

    {
        path: '/products/:id',
        name: 'products.edit',
        component: ProductsCreate,
    },

    {
        path: '/video-link',
        name: 'video-link',
        component: VideoLink,
    },
]