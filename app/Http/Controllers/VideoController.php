<?php

namespace App\Http\Controllers;

use App\Models\FileUploads;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() 
    {
        return view('video-link');
    }

    public function video()
    {
        $video = FileUploads::latest()->first();
        return 'videos/' . $video->name;
    }

    public function upload(Request $request)
    {
        $videoName = time().rand(1,99).'.'.$request->video->extension();  
        $path = $request->video->move(public_path('videos'), $videoName);

        $vids = [
            'name' => $videoName,
            'product_id' => 0,
            'path' => $path,
        ];

        FileUploads::create($vids);

        return 'videos/' . $videoName;
    }
}
