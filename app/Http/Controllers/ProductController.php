<?php

namespace App\Http\Controllers;

use App\Models\FileUploads;
use App\Models\Products;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() 
    {
        return view('products');
    }

    public function list(Request $request)
    {
        return Products::latest()
                ->where(function($query) use ($request) {
                    if ($request->search) {
                        $query->where('name', 'LIKE', '%'. $request->search .'%');
                    }

                    if ($request->filter) {
                        $query->where('category', 'LIKE', '%'. $request->filter .'%');
                    }
                })
                ->paginate(10);
    }

    public function create(Request $request) 
    {
        if ($request->id) {
            $product = $this->updateProduct($request);
        } else {
            $product = $this->insertProduct($request);
        }

        $images = [];
        if ($request->image){
            foreach($request->image as $key => $image)
            {
                $imageName = time().rand(1,99).'.'.$image->extension();  
                $path = $image->move(public_path('images'), $imageName);
  
                $images[] = [
                    'name' => $imageName,
                    'product_id' => $product->id,
                    'path' => $path,
                ];
            }
        }
  
        foreach ($images as $key => $image) {
            FileUploads::create($image);
        }

    }

    public function updateProduct($request)
    {
        $product = Products::where('id', $request->id)->first();
        $product->update([
            'name' => $request->name,
            'category' => $request->category,
            'description' => $request->description,
            'date_time' => date('Y-m-d H:i:s', strtotime($request->dateTime)),
        ]);

        return $product;
    }

    public function insertProduct($request)
    {
        return Products::create([
            'name' => $request->name,
            'category' => $request->category,
            'description' => $request->description,
            'date_time' => date('Y-m-d H:i:s', strtotime($request->dateTime)),
        ]);
    }

    public function view(Request $request) {
        $product = Products::where('id', $request->id)->first();

        return $product;
    }

    public function remove(Request $request)
    {
        Products::where('id', $request->id)->delete();
    }
}
