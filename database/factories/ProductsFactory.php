<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $category = [
            'Food',
            'Car',
            'Candy',
            'Computer',
            'Motor'
        ];

        return [
            'name' => fake()->word(),
            'category' => $category[rand(0, 4)],
            'description' => fake()->paragraph(),
            'date_time' => fake()->dateTimeBetween('-2 week', '+2 week'),
        ];
    }
}
