<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ApplicationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// Route::get('{view}', ApplicationController::class)->where('view', '(.*)');

Route::post('/api/productsList', [ProductController::class, 'list']);
Route::post('/api/products/{id?}', [ProductController::class, 'create']);
Route::delete('/api/products/{id}', [ProductController::class, 'remove']);
Route::get('/api/products/{id}', [ProductController::class, 'view']);

Route::post('/api/upload', [VideoController::class, 'upload']);
Route::post('/api/latestVideo', [VideoController::class, 'video']);

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/products', [ProductController::class, 'index']);
Route::get('/products/create', [ProductController::class, 'index']);
Route::get('/products/{id}', [ProductController::class, 'index']);
Route::get('/video-link', [VideoController::class, 'index']);

Auth::routes();
